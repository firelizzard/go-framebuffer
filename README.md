# Go Frame Buffers for Linux

Support for drawing to a Linux frame buffer device from Go, *without any CGo*.

Inspired by [gonutz](https://github.com/gonutz/framebuffer) and
[bfanger](https://github.com/bfanger/framebuffer).
